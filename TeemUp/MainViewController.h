//
//  MainViewController.h
//  TeemUp
//
//  Created by Gerard Edward Gonzalez on 11/15/13.
//  Copyright (c) 2013 Gerard Edward Gonzalez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"

@interface MainViewController : UIViewController <UITableViewDelegate,UITableViewDataSource>

@property (strong, nonatomic) User *currentUser;
@property (strong, nonatomic) NSArray *userFriends;

@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;



@end
