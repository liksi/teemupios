//
//  MapViewController.m
//  TeemUp
//
//  Created by Gerard Edward Gonzalez on 1/6/14.
//  Copyright (c) 2014 Gerard Edward Gonzalez. All rights reserved.
//

#import "MapViewController.h"
#import "myAnnotation.h"
#import "Friend.h"

@interface MapViewController ()

@end

@implementation MapViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    CLLocationCoordinate2D zoomLocation;
    zoomLocation.latitude = self.mapView.userLocation.coordinate.latitude;
    zoomLocation.longitude = self.mapView.userLocation.coordinate.longitude;
    
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 500, 500);
    self.isZoomed = TRUE;
    
    [self.mapView setRegion:viewRegion animated:YES];
    [self.segmentedControl setSelectedSegmentIndex:0];
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.mapView.delegate = self;
    
    [self plotFriends];
    
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    self.mapView.centerCoordinate =
    userLocation.location.coordinate;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id)annotation {
    
    if([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    static NSString *identifier = @"myAnnotation";
    MKPinAnnotationView * annotationView = (MKPinAnnotationView*)[self.mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
    if (!annotationView)
    {
        
        annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
        annotationView.pinColor = MKPinAnnotationColorPurple;
        annotationView.animatesDrop = YES;
        annotationView.canShowCallout = YES;
    }
    
    else {
        
        annotationView.annotation = annotation;
        
    }
    
    annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    
    return annotationView;
}

#pragma mark- Buttons

- (IBAction)mapButton:(id)sender {
    
    if(self.isZoomed){
        
        MKUserLocation *userLocation = self.mapView.userLocation;
        MKCoordinateRegion region =
        MKCoordinateRegionMakeWithDistance (
                                            userLocation.location.coordinate, 1000000, 1000000);
        [self.mapView setRegion:region animated:NO];
        
        self.isZoomed = FALSE;
        
    }
    
    else{
        
        MKUserLocation *userLocation = self.mapView.userLocation;
        MKCoordinateRegion region =
        MKCoordinateRegionMakeWithDistance (
                                            userLocation.location.coordinate, 500, 500);
        [self.mapView setRegion:region animated:NO];
        
        self.isZoomed = TRUE;
        
    }
    
}

- (IBAction)refreshLocations:(id)sender {
    
    //update locations
    for (myAnnotation *currentAnnotation in self.myAnnotations){
        
        NSLog(@"Current Annotation Title: %@", currentAnnotation.title);
        
    }
    
}

- (IBAction)segmentedControlValueChanged:(id)sender
{

    switch (self.segmentedControl.selectedSegmentIndex)
    {
        case 0:
            NSLog(@"Segment 0 selected.");
            break;
            
        case 1:
            NSLog(@"Segment 1 selected.");
            [self performSegueWithIdentifier:@"eventsSegue" sender:self];
            break;
            
        default: 
            break; 
    }
    
}





#pragma mark- Friends Methods

- (void)plotFriends
{
    
    self.myAnnotations = [NSMutableArray new];
    
    for (Friend *myFriend in self.userFriends){
        
        NSLog(@"Plotting Friend: %@ at lat %f long %f",myFriend.name, [myFriend.latitude doubleValue], [myFriend.longitude doubleValue]);
        
        CLLocationCoordinate2D coordinate1;
        coordinate1.latitude = [myFriend.latitude doubleValue];
        coordinate1.longitude = [myFriend.longitude doubleValue];
        myAnnotation *annotation = [[myAnnotation alloc] initWithCoordinate:coordinate1 title:myFriend.name];
        
        [self.mapView addAnnotation:annotation];
        [self.myAnnotations addObject:annotation];
        
    }
    
}

@end









