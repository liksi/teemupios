//
//  Friend.m
//  TeemUp
//
//  Created by Gerard Edward Gonzalez on 12/21/13.
//  Copyright (c) 2013 Gerard Edward Gonzalez. All rights reserved.
//

#import "Friend.h"
#import "User.h"


@implementation Friend

@dynamic available;
@dynamic identifier;
@dynamic latitude;
@dynamic longitude;
@dynamic name;
@dynamic phone_number;
@dynamic status;
@dynamic will_travel;
@dynamic user;

@end
