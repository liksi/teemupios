//
//  MainViewController.m
//  TeemUp
//
//  Created by Gerard Edward Gonzalez on 11/15/13.
//  Copyright (c) 2013 Gerard Edward Gonzalez. All rights reserved.
//

#import "MainViewController.h"
#import "AppDelegate.h"
#import "JSONTeemUp.h"
#import "Friend.h"
#import "MapViewController.h"

@interface MainViewController ()

@end

@implementation MainViewController
@synthesize currentUser;

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"mapSegue"]) {
        
        MapViewController *destViewController = segue.destinationViewController;
        destViewController.userFriends = self.userFriends;        
        
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [JSONTeemUp addFriendsWithToken:currentUser.authentication_token withIdentifier:currentUser.identifier inContext:self.managedObjectContext];
    self.userFriends = [currentUser.friends allObjects];
    
    self.navigationItem.hidesBackButton=YES;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotate
{
    return NO;
}

#pragma mark Table View Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [currentUser.friends count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier = @"FriendsCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];

    if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
       }
    
    Friend *currentFriend = [self.userFriends objectAtIndex:indexPath.row];
    
    [cell.textLabel setText:currentFriend.name];
    [cell.detailTextLabel setText:currentFriend.status];
    
    return cell;
    
}


@end








