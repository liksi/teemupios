//
//  FirstViewController.m
//  TeemUp
//
//  Created by Gerard Edward Gonzalez on 11/15/13.
//  Copyright (c) 2013 Gerard Edward Gonzalez. All rights reserved.
//

#import "FirstViewController.h"
#import "MainViewController.h"
#import "AppDelegate.h"
#import "User.h"
#import "JSONTeemUp.h"

@interface FirstViewController ()

@end

@implementation FirstViewController
@synthesize emailOutlet, passwordOutlet;

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"loginSegue"]) {
        
        //get user from the database by defaults ID
        AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        User *myUser = [appDelegate getUserByIdentifier:(NSNumber *)[defaults objectForKey:@"identifier"]];
        
        MainViewController *destViewController = segue.destinationViewController;
        destViewController.currentUser = myUser;
        destViewController.managedObjectContext = self.managedObjectContext;
        

    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *authToken = [defaults objectForKey:@"authentication_token"];
    
    //Authentication token is present, skip to main view
//    if(authToken != NULL){
//        
//        NSLog(@"Auth token: %@", authToken);
//        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
//        MainViewController *mvc = [storyboard instantiateViewControllerWithIdentifier:@"MainView"];
//        [self presentViewController:mvc animated:NO completion:nil];
//        
//        //grab user from current NSUserDefaults ID
//        AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
//        
//        NSLog(@"FVC identifier: %@", [defaults objectForKey:@"identifier"]);
//        
//        User *myUser = [appDelegate getUserByIdentifier:(NSNumber *)[defaults objectForKey:@"identifier"]];
//        
//        if(myUser == NULL){
//            
//            NSLog(@"USER IS NULL");
//        }
//        else{
//            
//            NSLog(@"User Auth Token: %@, User identifier: %@, User name: %@", myUser.authentication_token, myUser.identifier, myUser.name);
//            
//        }
//        
//    }
    
    
    //grab pointer to managed object context
    AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
    self.managedObjectContext = appDelegate.managedObjectContext;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) alertStatus:(NSString *)msg :(NSString *)title
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:msg
                                                       delegate:self
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil, nil];
    
    [alertView show];
}

#pragma loginButton

- (IBAction)loginButton:(id)sender {
    
    //Check if email or password is empty, else login
//    if([emailOutlet.text length] == 0)
//        [self alertStatus:@"Please Enter Email" :@"Login Failed!"];
//    else if([passwordOutlet.text length] == 0)
//        [self alertStatus:@"Please Enter Password" :@"Login Failed!"];
    
    //Post login credentials to URL
    //uncomment else when need checks
//    else{
    
    NSString *response = [JSONTeemUp loginWithEmail:@"gedward@gmail.com" withPassword:@"bananas99" inContext:self.managedObjectContext];
    
    //if successful login, go to next view
    if([response isEqualToString:@"success"]){
        [self performSegueWithIdentifier:@"loginSegue" sender:sender];
    }
    
//    }

    
}

- (IBAction)backgroundClick:(id)sender {
    [emailOutlet resignFirstResponder];
    [passwordOutlet resignFirstResponder];
    
}
@end










