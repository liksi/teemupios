//
//  JSONTeemUp.m
//  TeemUp
//
//  Created by Gerard Edward Gonzalez on 10/25/13.
//  Copyright (c) 2013 Gerard Edward Gonzalez. All rights reserved.
//
//  This class's purpose is to separate the JSON POSTing and Parsing used in this project.
//  There will be methods for registering, logging in, getting coordinates, sending coordinates, managing friends, etc.
//  Source: http://dipinkrishna.com/blog/2012/03/iphoneios-programming-login-screen-post-data-url-parses-json-response/5/

#import "JSONTeemUp.h"
#import "SBJson.h"
#import "AppDelegate.h"

#define REGISTER_URL @"http://developer.itsmarc.us:3000/api/tokens/sign_up.json"
#define LOGIN_URL @"http://developer.itsmarc.us:3000/api/tokens.json"
#define ALL_FRIENDS_URL @"http://developer.itsmarc.us:3000/api/friendships.json?token="

@implementation JSONTeemUp

+ (NSString *)registerUserWithEmail:(NSString *)userEmail
                       withPassword:(NSString *)userPassword
                          withPhone:(NSString *)userPhone
                          inContext:(NSManagedObjectContext *)managedObjectContext{
    
    NSString *post =[[NSString alloc] initWithFormat:@"{\"email\":\"%@\",\"password\":\"%@\",\"phone\":\"%@\"}",userEmail,userPassword,userPhone];
    
    NSLog(@"PostData: %@",post);
    
    NSURL *url=[NSURL URLWithString:REGISTER_URL];
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSError *error = [[NSError alloc] init];
    NSHTTPURLResponse *response = nil;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    NSLog(@"Response code: %ld", (long)[response statusCode]);
    
    if ([response statusCode] >=200 && [response statusCode] <300)
    {
        NSString *responseData = [[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
        NSLog(@"Response ==> %@", responseData);
        
        SBJsonParser *jsonParser = [SBJsonParser new];
        NSDictionary *jsonData = (NSDictionary *) [jsonParser objectWithString:responseData error:nil];
        NSLog(@"%@",jsonData);
        
        //Store the auth token in NSUserDefaults
        
        NSString *authToken = (NSString *)[[jsonData objectForKey:@"user"] objectForKey:@"authentication_token"];
        
        NSNumber *identifier = (NSNumber *)[[jsonData objectForKey:@"user"] objectForKey:@"id"];
        
        NSNumber *available = (NSNumber *)[[jsonData objectForKey:@"user"] objectForKey:@"available"];
        
        NSNumber *will_travel = (NSNumber *)[[jsonData objectForKey:@"user"] objectForKey:@"will_travel"];
        
        NSString *phone = (NSString *)[[jsonData objectForKey:@"user"] objectForKey:@"phone_number"];
        
        NSString *name = (NSString *)[[jsonData objectForKey:@"user"] objectForKey:@"name"];
        
        //JSON parsed, add to core data
        User *newUser = [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:managedObjectContext];
        
        newUser.authentication_token = authToken;
        newUser.identifier = identifier;
        newUser.phone_number = phone;
        newUser.name = name;
        newUser.will_travel = will_travel;
        newUser.available = available;
        
        NSError *error;
        if (![managedObjectContext save:&error]) {
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        }
        
        //also save auth_token and user_id to NSUserDefaults
        
        NSLog(@"Register identifier: %@", identifier);
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:authToken forKey:@"authentication_token"];
        [defaults setObject:identifier forKey:@"identifier"];
        [defaults synchronize];
        NSLog(@"Authentication Token Saved!");
        
        return @"success";
        
    }
    
    else if([response statusCode] == 401){
        return @"DataBase Error!";
    }
    
    else if([response statusCode] == 406){
        return @"Incorrect JSON format!";
    }
    
    else if([response statusCode] == 422){
        return @"Password Too Short!";
    }
    
    return @"Unknown Failure";
    
}

+ (NSString *)loginWithEmail:(NSString *)userEmail
            withPassword:(NSString *)userPassword
               inContext:(NSManagedObjectContext *)managedObjectContext{
    
    NSString *post =[[NSString alloc] initWithFormat:@"{\"email\":\"%@\",\"password\":\"%@\"}",userEmail,userPassword];
    
    NSLog(@"PostData: %@",post);
    
    NSURL *url=[NSURL URLWithString:LOGIN_URL];
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSError *error = [[NSError alloc] init];
    NSHTTPURLResponse *response = nil;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    NSLog(@"Response code: %ld", (long)[response statusCode]);
    
    if ([response statusCode] >=200 && [response statusCode] <300)
    {
        
        NSString *responseData = [[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
        NSLog(@"Response ==> %@", responseData);
        
        SBJsonParser *jsonParser = [SBJsonParser new];
        NSDictionary *jsonData = (NSDictionary *) [jsonParser objectWithString:responseData error:nil];
        NSLog(@"%@",jsonData);
        
        //Store the auth token in NSUserDefaults
        
        NSString *authToken = (NSString *)[[jsonData objectForKey:@"user"] objectForKey:@"authentication_token"];
        
        NSNumber *identifier = (NSNumber *)[[jsonData objectForKey:@"user"] objectForKey:@"id"];
        
        NSNumber *available = (NSNumber *)[[jsonData objectForKey:@"user"] objectForKey:@"available"];
        
        NSNumber *will_travel = (NSNumber *)[[jsonData objectForKey:@"user"] objectForKey:@"will_travel"];
        
        NSString *phone = (NSString *)[[jsonData objectForKey:@"user"] objectForKey:@"phone_number"];
        
        NSString *name = (NSString *)[[jsonData objectForKey:@"user"] objectForKey:@"name"];
        
        //check if user already exists. If so, delete before we add it to database
        AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
        
        if([appDelegate userExists:identifier]){
            [appDelegate deleteUserByIdentifier:identifier];
        }
        
        //JSON parsed, add to core data
        User *newUser = [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:managedObjectContext];
        
        newUser.authentication_token = authToken;
        newUser.identifier = identifier;
        newUser.phone_number = phone;
        newUser.name = name;
        newUser.will_travel = will_travel;
        newUser.available = available;
        
        NSError *error;
        if (![managedObjectContext save:&error]) {
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        }
        
        //also save auth_token and user_id to NSUserDefaults
        //we will use this to get access to the user in the database
        
        NSLog(@"Register identifier: %@", identifier);
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:authToken forKey:@"authentication_token"];
        [defaults setObject:identifier forKey:@"identifier"];
        [defaults synchronize];
        NSLog(@"Authentication Token Saved!");
        
        //add user's friends to the database
        //[self getFriendsWithToken:authToken];
        
        return @"success";
        
    }
    
    else if([response statusCode] == 401){
        return @"DataBase Error!";
    }
    
    else if([response statusCode] == 406){
        return @"Incorrect JSON format!";
    }
    
    else if([response statusCode] == 422){
        return @"Email Already in Use!";
    }
    
    return @"Unknown Failure";

}

+ (NSMutableArray *)addFriendsWithToken : (NSString *)userToken
                         withIdentifier : (NSNumber *)identifier
                              inContext : (NSManagedObjectContext *)managedObjectContext{
    
    NSString *urlString = [ALL_FRIENDS_URL stringByAppendingString:userToken];
    
    NSLog(@"urlString: %@",urlString);
    
    NSURL *url=[NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    NSError *error = [[NSError alloc] init];
    NSHTTPURLResponse *response = nil;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    NSLog(@"Response code: %ld", (long)[response statusCode]);
    
    if ([response statusCode] >=200 && [response statusCode] <300)
    {
        NSString *responseData = [[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
        NSLog(@"Response ==> %@", responseData);
        
        SBJsonParser *jsonParser = [SBJsonParser new];
        NSDictionary *jsonData = (NSDictionary *) [jsonParser objectWithString:responseData error:nil];
        
        NSLog(@"%@",jsonData);
        
        NSArray *userFriends = [jsonData objectForKey:@"friends"];
        
        //get the user so we can add the friends to the relationship
        AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
        User *myUser = [appDelegate getUserByIdentifier:identifier];
        
        //add the friends to the user in the database
        for(NSDictionary *friend in userFriends){
            
            NSLog(@"%@",[friend objectForKey:@"name"]);
            
            NSNumber *available = (NSNumber *)[friend objectForKey:@"available"];
            NSNumber *identifier = (NSNumber *)[friend objectForKey:@"id"];
            NSNumber *latitude = (NSNumber *)[friend objectForKey:@"latitude"];
            NSNumber *longitude = (NSNumber *)[friend objectForKey:@"longitude"];
            NSString *name = (NSString *)[friend objectForKey:@"name"];
            NSString *phone = (NSString *)[friend objectForKey:@"phone_number"];
            NSString *status = (NSString *)[friend objectForKey:@"status_message"];
            NSNumber *will_travel = (NSNumber *)[friend objectForKey:@"will_travel"];
            
            Friend *newFriend = [NSEntityDescription insertNewObjectForEntityForName:@"Friend" inManagedObjectContext:managedObjectContext];
            
            newFriend.available = available;
            newFriend.identifier = identifier;
            newFriend.latitude = latitude;
            newFriend.longitude = longitude;
            newFriend.name = name;
            newFriend.phone_number = phone;
            newFriend.status = status;
            newFriend.will_travel = will_travel;
            
            [myUser addFriendsObject:newFriend];

            if (![managedObjectContext save:&error]) {
                NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
            }
            
        }
        
    }
    
    return NULL;
}


@end


















