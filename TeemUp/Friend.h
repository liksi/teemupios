//
//  Friend.h
//  TeemUp
//
//  Created by Gerard Edward Gonzalez on 12/21/13.
//  Copyright (c) 2013 Gerard Edward Gonzalez. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class User;

@interface Friend : NSManagedObject

@property (nonatomic, retain) NSNumber * available;
@property (nonatomic, retain) NSNumber * identifier;
@property (nonatomic, retain) NSNumber * latitude;
@property (nonatomic, retain) NSNumber * longitude;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * phone_number;
@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSNumber * will_travel;
@property (nonatomic, retain) User *user;

@end
