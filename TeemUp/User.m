//
//  User.m
//  TeemUp
//
//  Created by Gerard Edward Gonzalez on 12/21/13.
//  Copyright (c) 2013 Gerard Edward Gonzalez. All rights reserved.
//

#import "User.h"
#import "Friend.h"


@implementation User

@dynamic authentication_token;
@dynamic available;
@dynamic creation_success;
@dynamic identifier;
@dynamic latitude;
@dynamic longitude;
@dynamic name;
@dynamic phone_number;
@dynamic status;
@dynamic will_travel;
@dynamic friends;

@end
