//
//  MapViewController.h
//  TeemUp
//
//  Created by Gerard Edward Gonzalez on 1/6/14.
//  Copyright (c) 2014 Gerard Edward Gonzalez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface MapViewController : UIViewController <MKMapViewDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@property (nonatomic) BOOL isZoomed;
@property (strong, nonatomic) NSArray *userFriends;
@property (strong, nonatomic) NSMutableArray *myAnnotations;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;

- (IBAction)mapButton:(id)sender;
- (IBAction)refreshLocations:(id)sender;
- (IBAction)segmentedControlValueChanged:(id)sender;



@end
