//
//  MasterViewController.h
//  TeemUp
//
//  Created by Gerard Edward Gonzalez on 10/14/13.
//  Copyright (c) 2013 Gerard Edward Gonzalez. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <CoreData/CoreData.h>

@interface MasterViewController : UITableViewController <NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end
