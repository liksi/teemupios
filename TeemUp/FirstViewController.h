//
//  FirstViewController.h
//  TeemUp
//
//  Created by Gerard Edward Gonzalez on 11/15/13.
//  Copyright (c) 2013 Gerard Edward Gonzalez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FirstViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *emailOutlet;
@property (weak, nonatomic) IBOutlet UITextField *passwordOutlet;

@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;

- (IBAction)loginButton:(id)sender;
- (IBAction)backgroundClick:(id)sender;

@end
