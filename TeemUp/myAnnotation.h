//
//  myAnnotation.h
//  TeemUp
//
//  Created by Gerard Edward Gonzalez on 1/8/14.
//  Copyright (c) 2014 Gerard Edward Gonzalez. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface myAnnotation : NSObject <MKAnnotation>

@property (nonatomic, copy) NSString *title;
@property (nonatomic,assign) CLLocationCoordinate2D coordinate;

-(id) initWithCoordinate:(CLLocationCoordinate2D)coordinate title:(NSString *)title;

@end
