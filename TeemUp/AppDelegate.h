//
//  AppDelegate.h
//  TeemUp
//
//  Created by Gerard Edward Gonzalez on 10/14/13.
//  Copyright (c) 2013 Gerard Edward Gonzalez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

//CRUD
-(User *)getUserByIdentifier : (NSNumber *)identifier;
-(BOOL)userExists : (NSNumber *)identifier;
-(void)deleteUserByIdentifier : (NSNumber *)identifier;

-(void)addUserWithToken : (NSString *)token
        withIdentifier  : (NSNumber *)identifier
              withPhone : (NSNumber *)phone
               withName : (NSString *)name
         withWillTravel : (NSNumber *)willTravel
          withAvailable : (NSNumber *)available;



@end
