//
//  JSONTeemUp.h
//  TeemUp
//
//  Created by Gerard Edward Gonzalez on 10/25/13.
//  Copyright (c) 2013 Gerard Edward Gonzalez. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
#import "Friend.h"

@interface JSONTeemUp : NSObject

@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;

+ (NSString *)registerUserWithEmail : (NSString *)userEmail
                       withPassword : (NSString *)userPassword
                       withPhone    : (NSString *)userPhone
                       inContext    : (NSManagedObjectContext *)managedObjectContext;

+ (NSString *)loginWithEmail : (NSString *)userEmail
                withPassword : (NSString *)userPassword
                inContext    : (NSManagedObjectContext *)managedObjectContext;

+ (NSMutableArray *)addFriendsWithToken : (NSString *)userToken
                         withIdentifier : (NSNumber *)identifier
                              inContext : (NSManagedObjectContext *)managedObjectContext;

@end
