//
//  myAnnotation.m
//  TeemUp
//
//  Created by Gerard Edward Gonzalez on 1/8/14.
//  Copyright (c) 2014 Gerard Edward Gonzalez. All rights reserved.
//

#import "myAnnotation.h"

@implementation myAnnotation

-(id) initWithCoordinate:(CLLocationCoordinate2D)coordinate title:(NSString *)title {
    if ((self = [super init])) {
        self.coordinate = coordinate;
        self.title = title;
    }
    return self;
}

@end
