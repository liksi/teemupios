//
//  RegisterViewController.m
//  TeemUp
//
//  Created by Gerard Edward Gonzalez on 10/17/13.
//  Copyright (c) 2013 Gerard Edward Gonzalez. All rights reserved.
//

#import "RegisterViewController.h"
#import "SBJson.h"
#import "JSONTeemUp.h"
#import "User.h"
#import "AppDelegate.h"
#import "User.h"

#define REGISTER_URL @"http://developer.itsmarc.us:3000/api/tokens/sign_up.json"

@interface RegisterViewController ()

@end

@implementation RegisterViewController
@synthesize textEmail, textPassword, textPasswordConf, textPhone;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
    self.managedObjectContext = appDelegate.managedObjectContext;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) alertStatus:(NSString *)msg :(NSString *)title
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:msg
                                                       delegate:self
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil, nil];
    
    [alertView show];
}

//--------------------------------
// This method posts the register JSON and on a success returns an authentication token
//--------------------------------

- (IBAction)buttonRegister:(id)sender {
    
    //Send JSON info and login if register info is valid
    
    BOOL passMatch = TRUE;
    
    if(passMatch == TRUE) {
        
        NSString *response = [JSONTeemUp registerUserWithEmail:@"gedward@gmail.com" withPassword:@"bananas99" withPhone:@"9492923177" inContext:self.managedObjectContext];
        
        if([response  isEqual: @"success"]){
            //switch scenes
            
            
            
        }
        
        else{
            //alert popup, clear screen
            [self alertStatus:response :@"Registration Failed!"];
            
        }
        
    }
    
    
}

- (IBAction)backgroundClick:(id)sender {
    [textEmail resignFirstResponder];
    [textPassword resignFirstResponder];
    [textPasswordConf resignFirstResponder];
    [textPhone resignFirstResponder];
}
@end
