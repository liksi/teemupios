//
//  RegisterViewController.h
//  TeemUp
//
//  Created by Gerard Edward Gonzalez on 10/17/13.
//  Copyright (c) 2013 Gerard Edward Gonzalez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *textEmail;
@property (weak, nonatomic) IBOutlet UITextField *textPassword;
@property (weak, nonatomic) IBOutlet UITextField *textPasswordConf;
@property (weak, nonatomic) IBOutlet UITextField *textPhone;

@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;

- (IBAction)buttonRegister:(id)sender;
- (IBAction)backgroundClick:(id)sender;

@end
