//
//  DetailViewController.h
//  TeemUp
//
//  Created by Gerard Edward Gonzalez on 10/14/13.
//  Copyright (c) 2013 Gerard Edward Gonzalez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (strong, nonatomic) id detailItem;

@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
@end
